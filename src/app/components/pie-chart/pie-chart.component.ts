import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  public pieChartLabels = ['Sales Q1', 'Sales Q2'];
  public pieChartData = [120, 150];
  public pieChartType = 'pie';

  constructor() {

  }

  ngOnInit() {
  }

}
